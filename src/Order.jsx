import { Button, FormControl, FormLabel, HStack, Icon, Input, Spacer, Stack } from '@chakra-ui/react'
import { useState } from 'react'
import { IoTicket, IoFileTrayFull } from 'react-icons/io5'
import { useNavigate } from 'react-router-dom'
import { casClient } from './CAS'

export const Order = () => {
  const [input, setInput] = useState({ name: '', surname: '', email: '' })
  const navigate = useNavigate()

  const login = async () => {
    try {
      const response = await casClient.auth()
      console.log(response)
    } catch (error) {
      console.log(error)
    }
  }

  const logout = async () => casClient.logout('/logout')

  const createTicket = async () => {
    if (isValidForm) {
      try {
        const ticket = await (
          await fetch(`https://api.passslot.com/v1/templates/${import.meta.env.VITE_TEMPLATE}/pass`, {
            method: 'POST',
            headers: { Authorization: import.meta.env.VITE_SECRET },
            body: JSON.stringify(input),
          })
        ).json()
        window.location = `https://api.fepoulpa.net/email/${ticket.serialNumber}`
      } catch (error) {
        alert("Une erreur s'est produite :" + error)
      }
    }
  }

  const isValidEmail = email =>
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      String(email).toLowerCase()
    )

  const isValidForm = isValidEmail(input.email) && !Object.keys(input).some(key => input[key] === '')

  return (
    <Stack p="4">
      <HStack>
        <FormControl>
          <FormLabel>Prénom</FormLabel>
          <Input
            value={input.name}
            onChange={e =>
              setInput({ ...input, name: e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1) })
            }
          />
        </FormControl>
        <Spacer />
        <FormControl>
          <FormLabel>Nom</FormLabel>
          <Input
            value={input.surname}
            onChange={e =>
              setInput({ ...input, surname: e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1) })
            }
          />
        </FormControl>
      </HStack>
      <FormControl>
        <FormLabel>Email</FormLabel>
        <Input
          type="email"
          value={input.email}
          onChange={e => setInput({ ...input, email: e.target.value })}
          onKeyPress={e => e.key === 'Enter' && createTicket()}
          isInvalid={input.email !== '' && !isValidEmail(input.email)}
        />
      </FormControl>
      <Spacer />
      <Button onClick={createTicket} isDisabled={!isValidForm} colorScheme="teal" rightIcon={<Icon as={IoTicket} />}>
        Prendre une place
      </Button>
      <Spacer />
      <Button
        onClick={() => navigate('/tickets')}
        colorScheme="teal"
        variant="link"
        rightIcon={<Icon as={IoFileTrayFull} />}
      >
        Retrouver un de vos tickets
      </Button>
      <Button onClick={login} colorScheme="teal" variant="link" rightIcon={<Icon as={IoFileTrayFull} />}>
        Connection CAS UTC
      </Button>
      <Button onClick={logout} colorScheme="teal" variant="link" rightIcon={<Icon as={IoFileTrayFull} />}>
        Déconnexion
      </Button>
    </Stack>
  )
}
