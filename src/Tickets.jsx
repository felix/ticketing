import { Button, FormControl, FormLabel, Icon, Input, Stack } from '@chakra-ui/react'
import { useState } from 'react'
import { IoArrowForward } from 'react-icons/io5'
import { useNavigate } from 'react-router-dom'

export const Tickets = () => {
  const [input, setInput] = useState('')
  const navigate = useNavigate()

  return (
    <Stack p={4}>
      <FormControl>
        <FormLabel>Entrez la référence de votre billet</FormLabel>
        <Input value={input} onChange={e => setInput(e.target.value.replace(/[^0-9a-z-]+/g, ''))} />
      </FormControl>
      <Button onClick={() => navigate(`/tickets/${input}`)} colorScheme="teal" rightIcon={<Icon as={IoArrowForward} />}>
        Continuer
      </Button>
    </Stack>
  )
}
