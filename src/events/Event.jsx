import { Button, Card, CardBody, CardFooter, Heading, Image, Stack, Text } from '@chakra-ui/react'

export const Event = ({ title, description, imageLink }) => (
  <Card direction={{ base: 'column', sm: 'row' }} overflow="hidden">
    <Image objectFit="cover" maxW={{ base: '100%', sm: '200px' }} src={imageLink} />
    <Stack>
      <CardBody>
        <Heading size="md">{title}</Heading>
        <Text py="2">{description}</Text>
      </CardBody>
      <CardFooter>
        <Button variant="solid" colorScheme="blue">
          Acheter un ticket
        </Button>
      </CardFooter>
    </Stack>
  </Card>
)
