import { useEffect } from 'react'
import { Event } from './Event'

const events = [
  {
    title: 'La Ménagerie de Verre',
    description: 'Isabelle Hubert & Cie',
    imageLink:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/L%E2%80%99Int%C3%A9rieur_de_chez_Bruant_-_Le_Mirliton%2C_by_Louis_Anquetin.jpg/800px-L%E2%80%99Int%C3%A9rieur_de_chez_Bruant_-_Le_Mirliton%2C_by_Louis_Anquetin.jpg',
  },
]

const getEvents = async () => {
  try {
    const response = await (
      await fetch('https://data.fepoulpa.net/jsonapi/node/spectacle', {
        headers: { Authorization: 'Basic ' + window.btoa('fepoulpa' + ':' + 'lq9_P9r0nP7') },
      })
    ).json()
    console.log(response)
  } catch (e) {
    alert('Something went wrong: ' + e)
  }
}

export const Events = () => {
  useEffect(() => {
    getEvents()
  }, [])

  return events.map(event => <Event key={event.title} {...event} />)
}
