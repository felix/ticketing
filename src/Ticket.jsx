import { Button, Icon, Progress, Stack, Text } from '@chakra-ui/react'
import { useEffect, useState } from 'react'
import { IoCheckmarkCircle, IoDownload, IoSad } from 'react-icons/io5'
import { useNavigate, useParams } from 'react-router-dom'

export const Ticket = ({ autoDownload }) => {
  const navigate = useNavigate()
  const { ticketId } = useParams()
  const [ticket, setTicket] = useState({})

  const checkTicket = async () => {
    try {
      setTicket(
        await (
          await fetch(`https://api.passslot.com/v1/passes/pass.slot.generic/${ticketId}/values`, {
            headers: { Authorization: import.meta.env.VITE_SECRET },
          })
        ).json()
      )
    } catch (e) {
      setTicket(null)
    }
  }

  const deleteTicket = async () => {
    try {
      const response = await fetch(`https://api.passslot.com/v1/passes/pass.slot.eventticket/${ticketId}/url`, {
        headers: {
          Authorization: import.meta.env.VITE_SECRET,
        },
      })
      console.log(response)
    } catch (e) {
      alert('Something went wrong: ' + e)
    }
  }

  const downloadTicket = async () =>
    window.location.assign(
      window.URL.createObjectURL(
        await (
          await fetch(`https://api.passslot.com/v1/passes/pass.slot.generic/${ticketId}`, {
            headers: { Authorization: import.meta.env.VITE_SECRET },
          })
        ).blob()
      )
    )

  useEffect(() => {
    checkTicket()
    if (autoDownload) downloadTicket()
  }, [])

  if (!ticket)
    return (
      <Button m={8} colorScheme="orange" variant="link" _hover={{ cursor: 'default' }} leftIcon={<Icon as={IoSad} />}>
        Aucun ticket ne correspond à ce numéro !
      </Button>
    )

  return Object.keys(ticket).length ? (
    <Stack p="4">
      <Button
        m={8}
        colorScheme="green"
        variant="link"
        _hover={{ cursor: 'default' }}
        leftIcon={<Icon as={IoCheckmarkCircle} />}
      >
        Bonjour {ticket.name.charAt(0).toUpperCase() + ticket.name.slice(1)} ! Votre ticket est disponible !
      </Button>
      {(/(android)/i.test(navigator.userAgent) && (
        <Button variant="ghost" disabled colorScheme="teal" rightIcon={<Icon as={IoDownload} />}>
          Sauvegarde sur Android non disponible
        </Button>
      )) ||
        ((/(iPad|iPhone|iPod)/g.test(navigator.userAgent) || window.safari !== undefined) && (
          <Button onClick={downloadTicket} colorScheme="teal" rightIcon={<Icon as={IoDownload} />}>
            Sauvegarder mon ticket
          </Button>
        )) || <Text align="center">Pour récupérer votre billet, accédez à cette page depuis Safari ou un iPhone</Text>}
    </Stack>
  ) : (
    <Progress isIndeterminate />
  )
}
