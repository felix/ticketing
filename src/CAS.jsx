import CasClient from './CAS/index'

let casEndpoint = 'cas.utc.fr'
let casOptions = {}

export const casClient = new CasClient(casEndpoint, casOptions)
