import { Card, Center, Image } from '@chakra-ui/react'
import { Route, Routes } from 'react-router-dom'
import { Events } from './events/Events'
import { Order } from './Order'
import { Ticket } from './Ticket'
import { Tickets } from './Tickets'

export const App = () => (
  <Center h="100vh" bgGradient="linear(to-r, gray.300, yellow.400, pink.200)">
    <Card direction={{ base: 'column', sm: 'row' }} overflow="hidden">
      <Routes>
        <Route path="/" element={<Order />}></Route>
        <Route path="events" element={<Events />}></Route>
        <Route path="tickets" element={<Tickets />}></Route>
        <Route path="tickets/:ticketId" element={<Ticket />}></Route>
        <Route path="tickets/:ticketId/download" element={<Ticket autoDownload />}></Route>
      </Routes>
    </Card>
  </Center>
)
