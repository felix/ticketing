import buildUrl from 'build-url'
import { getFullProtocol, isEmpty, isParamExistsInUrl, throwError } from './util'
import { CAS_STATUS_IN_PROCESS, CAS_VERSION_2_0, CAS_VERSION_3_0 } from './constant'

const getLoginUrl = (cas, gateway = false) => {
  let baseUrl = _getCasBaseUrl(cas)

  let queryParams = !isParamExistsInUrl(cas.redirectUrl, 'status')
    ? {
        service: buildUrl(cas.redirectUrl, {
          queryParams: {
            status: CAS_STATUS_IN_PROCESS,
          },
        }),
      }
    : {
        service: buildUrl(cas.redirectUrl),
      }

  if (gateway) {
    queryParams.gateway = true
  }

  switch (cas.version) {
    case CAS_VERSION_2_0:
    case CAS_VERSION_3_0:
      return buildUrl(baseUrl, {
        path: 'login',
        queryParams: queryParams,
      })

    default:
      throw throwError('Unsupported CAS Version')
  }
}

const getLogoutUrl = (cas, redirectPath = '') => {
  let baseUrl = _getCasBaseUrl(cas)

  let redirectUrl = buildUrl(window.location.origin, {
    path: redirectPath,
  })
  let queryParams = {}

  switch (cas.version) {
    case CAS_VERSION_2_0:
      if (!isEmpty(redirectPath)) {
        queryParams = {
          url: redirectUrl,
        }
      }

      break

    case CAS_VERSION_3_0:
      if (!isEmpty(redirectPath)) {
        queryParams = {
          service: redirectUrl,
        }
      }

      break

    default:
      throw throwError('Unsupported CAS Version')
  }

  let params = {
    path: 'logout',
  }

  if (Object.keys(queryParams).length !== 0) {
    params.queryParams = queryParams
  }

  return buildUrl(baseUrl, params)
}

const getValidateUrl = (cas, ticket) => {
  let baseUrl = _getCasBaseUrl(cas, true)

  let queryParams = {
    service: cas.redirectUrl,
    ticket: ticket,
  }
  let path = ''

  switch (cas.version) {
    case CAS_VERSION_2_0:
      path = 'serviceValidate'
      break

    case CAS_VERSION_3_0:
      path = 'p3/serviceValidate'
      queryParams.format = 'json'
      break

    default:
      throw throwError('Unsupported CAS Version')
  }

  if (!isEmpty(cas.proxy_callback_url)) {
    queryParams.pgtUrl = cas.proxy_callback_url
  }

  return buildUrl(baseUrl, {
    path: path,
    queryParams: queryParams,
  })
}

const _getCasBaseUrl = (cas, withProxyIfExists = false) => {
  if (withProxyIfExists && cas.validation_proxy) {
    const protocol = !isEmpty(cas.validation_proxy_protocol)
      ? getFullProtocol(cas.validation_proxy_protocol)
      : getFullProtocol(window.location.protocol)
    window.location.origin.replace(/(^\w+:|^)\/\//, '')
    const endpoint = !isEmpty(cas.validation_proxy_endpoint)
      ? cas.validation_proxy_endpoint
      : window.location.origin.replace(/(^\w+:|^)\/\//, '')
    return protocol + endpoint + cas.validation_proxy_path
  } else {
    return getFullProtocol(cas.protocol) + cas.endpoint + cas.path
  }
}

export { getLoginUrl, getLogoutUrl, getValidateUrl }
