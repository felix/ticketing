const isEmpty = obj => obj === undefined || obj === null || obj.toString().replace(/\s/g, '') === ''

const throwError = err => {
  throw new Error('[CasClient]: ' + err)
}

const getCurrentUrl = (withoutTicket = true) => {
  let url = window.location.href
  if (withoutTicket) return url.replace(/(^|[&?])ticket(=[^&]*)?/, '')
  return url
}

const getParamFromCurrentUrl = param => new URL(window.location.href).searchParams.get(param)

const getFullProtocol = protocol => (['http', 'http:'].includes(protocol) ? 'http://' : 'https://')

const isParamExistsInUrl = (url, param) => new URL(url).searchParams.get(param) !== null

export { isEmpty, throwError, getCurrentUrl, getParamFromCurrentUrl, getFullProtocol, isParamExistsInUrl }
