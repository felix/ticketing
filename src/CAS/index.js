import { getCurrentUrl, getParamFromCurrentUrl, isEmpty, throwError } from './util'
import { getLoginUrl, getLogoutUrl, getValidateUrl } from './url'
import {
  CAS_ERROR_AUTH_ERROR,
  CAS_ERROR_FETCH,
  CAS_ERROR_PARSE_RESPONSE,
  CAS_STATUS_IN_PROCESS,
  CAS_VERSIONS,
  CAS_VERSION_2_0,
  CAS_VERSION_3_0,
} from './constant'

const defaultOptions = {
  protocol: 'https',
  path: '/cas',
  version: CAS_VERSION_3_0,
  proxy_callback_url: '',
  validation_proxy: false,
  validation_proxy_protocol: '',
  validation_proxy_endpoint: '',
  validation_proxy_path: '',
}

class CasClient {
  constructor(endpoint, options = {}) {
    if (isEmpty(endpoint)) {
      throwError('Missing endpoint')
    }

    let version = options.version || defaultOptions.version

    if (!CAS_VERSIONS.includes(version)) {
      throwError('Unsupported CAS Version')
    }

    this.endpoint = endpoint
    this.path = options.path || defaultOptions.path
    this.protocol = options.protocol || defaultOptions.protocol
    this.version = options.version || defaultOptions.version
    this.proxy_callback_url = options.proxy_callback_url || defaultOptions.proxy_callback_url
    this.validation_proxy = options.validation_proxy || defaultOptions.validation_proxy
    this.validation_proxy_protocol = options.validation_proxy_protocol || defaultOptions.validation_proxy_protocol
    this.validation_proxy_endpoint = options.validation_proxy_endpoint || defaultOptions.validation_proxy_endpoint
    this.validation_proxy_path = options.validation_proxy_path || defaultOptions.validation_proxy_path
    this.redirectUrl = getCurrentUrl()
  }

  auth(gateway = false) {
    return new Promise((resolve, reject) => {
      /**
       * Save ticket to sessionStorage if exists
       */
      const ticket = getParamFromCurrentUrl('ticket')

      if (isEmpty(ticket)) {
        let status = getParamFromCurrentUrl('status')

        if (status === CAS_STATUS_IN_PROCESS) {
          this._handleFailsValdiate(reject, {
            type: CAS_ERROR_AUTH_ERROR,
            message: 'Missing ticket from return url',
          })
        } else {
          window.location.href = getLoginUrl(this, gateway)
        }
      } else {
        this._validateTicket(ticket, resolve, reject)
      }
    })
  }

  logout(redirectPath = '') {
    window.location.href = getLogoutUrl(this, redirectPath)
  }

  _getSuccessResponse(user, pgtIou = null) {
    let response = {
      currentUrl: window.location.origin + window.location.pathname,
      currentPath: window.location.pathname,
      user: user,
    }

    if (pgtIou) {
      response.pgtIou = pgtIou
    }

    return response
  }

  _validateTicket(ticket, resolve, reject) {
    let version = this.version
    let content_type

    switch (version) {
      case CAS_VERSION_2_0:
        content_type = 'text/xml'
        break

      case CAS_VERSION_3_0:
        content_type = 'application/json'
        break

      default:
        throw throwError('Unsupported CAS Version')
    }

    fetch(getValidateUrl(this, ticket), {
      headers: {
        'Content-Type': content_type,
      },
    })
      .then(
        function (response) {
          response
            .text()
            .then(
              function (text) {
                switch (version) {
                  case CAS_VERSION_3_0:
                    try {
                      let json = JSON.parse(text)

                      if (json.serviceResponse) {
                        if (json.serviceResponse.authenticationSuccess) {
                          let user = json.serviceResponse.authenticationSuccess.user
                          let pgtIou = null

                          if (!isEmpty(this.proxy_callback_url)) {
                            pgtIou = json.serviceResponse.authenticationSuccess.proxyGrantingTicket
                          }

                          this._handleSuccessValdiate(resolve, user, pgtIou)
                        } else {
                          this._handleFailsValdiate(reject, {
                            type: CAS_ERROR_AUTH_ERROR,
                            code: json.serviceResponse.authenticationFailure.code,
                            message: json.serviceResponse.authenticationFailure.description,
                          })
                        }
                      }
                    } catch (error) {
                      this._handleFailsValdiate(reject, {
                        type: CAS_ERROR_PARSE_RESPONSE,
                        message: 'Failed to parse response',
                        exception: error,
                      })
                    }

                    break

                  default:
                    throw throwError('Unsupported CAS Version')
                }

                throw throwError('Stop...')
              }.bind(this)
            )
            .catch(
              function (error) {
                this._handleFailsValdiate(reject, {
                  type: CAS_ERROR_PARSE_RESPONSE,
                  message: 'Failed to parse response',
                  exception: error,
                })
              }.bind(this)
            )
        }.bind(this)
      )
      .catch(
        function (error) {
          this._handleFailsValdiate(reject, {
            type: CAS_ERROR_FETCH,
            message: 'Failed to connect CAS server',
            exception: error,
          })
        }.bind(this)
      )
  }

  _handleSuccessValdiate(callback, user, pgtIou = null) {
    callback(this._getSuccessResponse(user, pgtIou))
  }

  _handleFailsValdiate(callback, error) {
    error.currentUrl = window.location.origin + window.location.pathname
    error.currentPath = window.location.pathname
    callback(error)
  }
}

export default CasClient